import React, {useState} from 'react';
import {Container} from 'react-bootstrap';
import MyNav from './components/MyNav';
import Video from "./components/Video";
import DetailedVideo from "./components/DetailedVideo";
import './App.css';

const App = () => {
    const [videos, setVideos] = useState([]);
    const [selectedVideo, selectVideo] = useState(null);

    return (
        <Container className="p-3">
            <MyNav onResults={setVideos}/>

            {selectedVideo != null &&
            <DetailedVideo id={selectedVideo.id}
                           snippet={selectedVideo.snippet} player={selectedVideo.player}
                           statistics={selectedVideo.statistics}
                           onEscape={() => selectVideo(null)}/>}

            {videos.length === 0 && selectedVideo == null && "Merci d'effectuer une recherche..."}

            {videos.length > 0 && selectedVideo == null
            && videos.map(v => {
                const {
                    title, description, thumbnails,
                    channelTitle, publishTime
                } = v.snippet;

                return (<Video
                    videoId={v.id.videoId}
                    thumbnail={thumbnails.high}
                    description={description}
                    channelTitle={channelTitle}
                    publishTime={publishTime}
                    title={title}
                    selectVideo={selectVideo}/>);
            })}
        </Container>
    );
};

export default App;
