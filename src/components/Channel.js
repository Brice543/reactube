import React from "react";
import Card from "react-bootstrap/Card";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Badge from "react-bootstrap/Badge";
import Button from "react-bootstrap/Button";

const Channel = ({id, snippet, player, statistics, onEscape}) => {
    const {title, channelTitle, description, publishTime, tags, thumbnails} = snippet;
    const {viewCount, likeCount, dislikeCount, commentCount, subscriberCount, videoCount} = statistics;


    return (<Card style={{width: '100%'}}>
        <Card.Body>
            <Button variant="danger" style={{float: 'right'}} onClick={onEscape}>Retour</Button>
            <Row>
                <Col width={"100%"}>
                    <div dangerouslySetInnerHTML={{
                        __html: player.embedHtml
                            .replace(/width="\d+"/, 'width="100%"')
                            .replace(/height="\d+"/, 'height="640"')
                    }}/>
                </Col>
            </Row>
            <Row>
                <h5 className="tags">{tags.map(tag => <><Badge variant="dark" pill>{tag}</Badge>{' '}</>)}</h5>
            </Row>
            <Row>
                <Col md={8}>
                    <h2>{title}</h2>
                    <h5 className="mb-2 text-info">{channelTitle}</h5>
                    <h5 className="mb-2 text-muted">
                        - {subscriberCount}
                        - {videoCount}
                        - {viewCount}
                    </h5>
                    <Card.Text dangerouslySetInnerHTML={{__html: description.replace(/\n/g, '<br/>')}}/>
                    <Card.Text className="mb-1 text-muted">{publishTime}</Card.Text>
                </Col>
            </Row>
        </Card.Body>
    </Card>);
}

export default Channel;
