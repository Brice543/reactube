import React, {useState} from 'react';
import {Button, Form, FormControl} from "react-bootstrap";
import api from '../lib/api';
import {useHistory} from "react-router-dom";

//Le onResults à ce niveau permet de refaire remonter les informatiosn dans les composants au dessus.
// Il devra être ajouté dans chaque composant au dessus de ce composant.
const SearchBar = ({onResults, selectedType}) => {
    const [typed, setTyped] = useState('');

    const search = async () => {
        const resp = await api.get('/search', {
            params: {
                q: typed,
                part: 'snippet',
                maxResults: 10,
                type: selectedType
            }
        });
        console.log('Received', resp.data.items);
        onResults(resp.data.items);
        //history.push(`/search/${selectedType}s/${typed}`); Erreur sur le history je ne comprends pas
};

    return (<Form inline>
        <FormControl type="text" placeholder="Search" className="mr-sm-2"
                     value={typed}onChange={event => setTyped(event.target.value)}/>
        <Button variant="outline-info" onClick={search}>Search</Button></Form>);
}

export default SearchBar;