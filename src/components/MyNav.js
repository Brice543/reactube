import Navbar from "react-bootstrap/Navbar";
import {Nav} from "react-bootstrap";
import SearchBar from './SearchBar';
import {useState} from "react";
import {
    useParams
} from "react-router-dom";

const MyNav = ({onResults}) => {

    const [type, setType] = useState('video');

    return (
        <Navbar bg="dark" variant="dark">
            <Navbar.Brand href="#home">Reactube</Navbar.Brand>
            <Nav className="mr-auto">
                <Nav.Link href="#home" onClick={() => {setType('video')} }>Videos</Nav.Link>
                <Nav.Link href="#features" onClick={() => {setType('channel')}}>Channels</Nav.Link>
            </Nav>
            <SearchBar onResults={onResults} selectedType={type}/>
        </Navbar>);
}
export default MyNav;